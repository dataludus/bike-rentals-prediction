In this project I use a Washington D.C. communal bike rental data.
This information was compiled by Hadi Fanaee-T at the University of Porto.

The information  organized in such a way that one row  corresponds to a single hour of a single day.

The goal of this project is to predict the total number of bikes  people rented in a given hour.
I use different  machine learning algorithms and check their performance.
When applicable, I'll be using a fixed seed for random algorithms, which makes the test reproducible.